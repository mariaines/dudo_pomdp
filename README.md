# Dudo POMDP: AA228 Final Project#

## About
This module is a student project for Stanford University's
[AA228: Decision Making Under Uncertainty](http://web.stanford.edu/class/aa228/#!index.md),
taught by [Mykel Kochenderfer](http://mykel.kochenderfer.com/) in fall 2015.

The project is to model [*Dudo*](https://en.wikipedia.org/wiki/Dudo), a
variant of Liar's Dice, as a 
[partially-observable Markov decision process](https://en.wikipedia.org/wiki/Partially_observable_Markov_decision_process)
(POMDP). Read the
[project proposal](https://drive.google.com/file/d/0BwMXqU-lTlG6bzd4dFNfUDhlaGM/view?usp=sharing)
and the
[final paper](https://drive.google.com/file/d/0BwMXqU-lTlG6MTF3UE8xT3hFUGxMVlVhS1RGNS0tcVVyMHY4/view?usp=sharing).

We define a POMDP interface for *Dudo* as defined by the
[POMDPs.jl](https://github.com/sisl/POMDPs.jl)
package, and provide a simple simulator for testing policies.

## Installation
```julia
Pkg.clone("https://mariaines@bitbucket.org/mariaines/dudo_pomdp.git")
```

## Usage
```julia
julia> using Dudo
julia> using SARSOP
```

### Modeling & solving
For a intro to SARSOP & POMDPs.jl, see the 
[POMDPs.jl tutorial](http://nbviewer.ipython.org/github/sisl/POMDPs.jl/blob/master/examples/Tiger.ipynb).

```julia
julia> pomdp = DudoPomdp(4,3)
DudoPomdp: {4-sided die, starting_hand_size: 3}

julia> pomdpfile = POMDPFile(pomdp, "dudo.pomdpx")
julia> solver = SARSOPSolver()
julia> policy = POMDPPolicy("dudo.policy")
julia> solve(solver, pomdpfile, policy)

julia> simulate(pomdp, policy, opponent=NaiveOpponent(), n_games=10, iterations=100)
```

### Play against a solved policy
```julia
julia> play(pomdp, policy)
New game! Each player starts with 1 6-sided dice
Hit enter to start.
>>
You get to start this game.

Your hand: [3]
Dice in play: 2
Current guess: DudoGuess: (0,0) (new_round)

New round, you must raise.
Enter count for guess (for six threes, type '6') (0:2)
>> 1
Enter value for guess (for six threes, type '3') (0:6)
>> 3

POMDP chose raise: DudoGuess: (1,6)

Your hand: [3]
Dice in play: 2
Current guess: DudoGuess: (1,6) (raised)

Raise or challenge? (type 'raise' or 'challenge')
>> challenge
Starting hands were ([6],[3])
Sorry, you lost!

Play again? [y for yes, n for no] (one of [yn])
>> 
```