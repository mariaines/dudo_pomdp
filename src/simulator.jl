
abstract PolicyHelper
get_action(ph::PolicyHelper, b::Belief) = error("get_action not implemented")

type QMDPPolicyHelper <: PolicyHelper
    policy::QMDPPolicy
end
get_action(ph::QMDPPolicyHelper, b::Belief) = action(ph.policy, b)

type SARSOPPolicyHelper <: PolicyHelper
    policy::POMDPPolicy
    action_map # type? todo
    SARSOPPolicyHelper(pomdp, policy) = new(policy, domain(actions(pomdp)))
end
get_action(ph::SARSOPPolicyHelper, b::Belief) = ph.action_map[action(ph.policy, b)]




function play(pomdp::DudoPomdp, policy::POMDPPolicy; iterations::Int=50, log::Bool=false, rly_log::Bool=false)
    play_continuous(pomdp, SARSOPPolicyHelper(pomdp, policy), iterations=iterations, log=log, rly_log=rly_log)
end
function play(pomdp::DudoPomdp, policy::QMDPPolicy; iterations::Int=50, log::Bool=false, rly_log::Bool=false)
    play_continuous(pomdp, QMDPPolicyHelper(policy), iterations=iterations, log=log, rly_log=rly_log)
end
function play_continuous(pomdp::DudoPomdp, policy_helper::PolicyHelper; iterations::Int=50, log::Bool=false, rly_log::Bool=false)
    simulate_once(pomdp, policy_helper, opponent=HumanOpponent(), iterations=iterations, log=log, rly_log=rly_log)
    cont = get_char("yn", "Play again? [y for yes, n for no]")
    while cont == 'y'
        simulate_once(pomdp, policy_helper, opponent=HumanOpponent(), iterations=iterations, log=log, rly_log=rly_log)
        cont = get_char("yn", "Play again? [y for yes, n for no]")
    end
    println("Goodbye")
end

function simulate(pomdp::DudoPomdp, policy::POMDPPolicy; opponent::Opponent=RandomOpponent(), 
        n_games::Int=1, iterations::Int=50, log::Bool=false, rly_log::Bool=false)
    simulate_n(pomdp, SARSOPPolicyHelper(pomdp, policy), opponent, n_games, iterations=iterations, log=log, rly_log=rly_log)
end
function simulate(pomdp::DudoPomdp, policy::QMDPPolicy; opponent::Opponent=RandomOpponent(),
        n_games::Int=1, iterations::Int=50, log::Bool=false, rly_log::Bool=false)
    simulate_n(pomdp, QMDPPolicyHelper(policy), opponent, n_games, iterations=iterations, log=log, rly_log=rly_log)
end

function simulate_n(pomdp::DudoPomdp, policy_helper::PolicyHelper, opponent::Opponent=RandomOpponent(), 
        n_games::Int=1; iterations::Int=50, log::Bool=false, rly_log::Bool=false)
    n_wins = 0
    for i in 1:n_games
        if i % 100 == 0 println("game $i") end
        if simulate_once(pomdp, policy_helper, opponent=opponent, iterations=iterations, log=log, rly_log=rly_log)
            n_wins += 1
        end
    end
    println("Won $n_wins of $n_games games ($(n_wins/n_games))")
end

function simulate_once(pomdp::DudoPomdp, policy_helper::PolicyHelper; opponent::Opponent=RandomOpponent(), 
        iterations::Int=50, log::Bool=false, rly_log::Bool=false)
    s = create_state(pomdp)
    o = create_observation(pomdp, s)

    b = initial_belief(pomdp)
    updater = DiscreteUpdater(pomdp) # this comes from POMDPToolbox
    b = POMDPToolbox.update(updater, b, DudoAction(end_round), o)

    rng = MersenneTwister() # initialize a random number generator

    rtot = 0.0

    original_hands = (s.my_hand, s.other_hand)
    if rly_log
        log = true
    end
    if typeof(opponent) == HumanOpponent
        if !rly_log
            log = false
        end
        println("New game! Each player starts with $(pomdp.starting_hand_size) $(pomdp.n_sides_die)-sided dice")
        input("Hit enter to start.")
        if s.myturn
            println("POMDP gets to start this game.")
        else
            println("You get to start this game.")
        end
    end

    for i = 1:iterations
        a = get_action(policy_helper, b)

        if log
            println("\nCurrent state: ")
            @show s
            println("\n\nCHOSE ACTION")
            @show a
        end

        # compute the reward
        r = reward(pomdp, s, a)
        if typeof(opponent) == HumanOpponent
            if s.myturn
                if a.action == raise
                    println("POMDP chose raise: $(a.raise)")
                elseif a.action == challenge
                    println("POMDP chose challenge")
                    if r > 0
                        println("You lost a challenge! You lose one die.")
                    else
                        println("POMDP lost a challenge! POMDP loses one die.")
                    end
                    println("\nNew round.")
                end
            end
        end

        rtot += r
        
        if log
            println("Time step $i")
            println("Have belief: $(b.b)")
            println("Taking action: $(a), got reward: $(r)")
        end
        # transition the system state
        trans_dist = transition(pomdp, s, a)
        if s.myturn || a.action != wait || is_game_over(s)
            rand!(rng, s, trans_dist)
        else
            if log println("Getting opponent's action...") end
            s = get_next_state(opponent, pomdp, opponent_obs_from_state(s), trans_dist)
        end
        if log
            @show trans_dist
            println("\n\nTRANSITIONING to")
            @show s
        end
        if is_terminal(s)
            if log println("POMDP is done after $i time steps") end
            break
        end

        # sample a new observation
        obs_dist = observation(pomdp, s, a)
        rand!(rng, o, obs_dist)
        if log
            println("\n\nSAMPLING NEW OBSERVATION from $obs_dist")
            @show o
        end
        
        # update the belief
        b = POMDPToolbox.update(updater, b, a, o)
        
        if log
            println("Saw observation: $(o), new belief: $(b.b)\n")
        end

    end
    if log println("Total reward: $rtot") end
    if typeof(opponent) == HumanOpponent
        if rtot > 0 
            println("Sorry, you lost!\n")
        else
            println("You beat the POMDP!\n")
        end
        println("Starting hands were $(original_hands)")
    end
    rtot > 0
end

function opponent_obs_from_state(s::DudoState)
    DudoObservation(s.other_hand, dice_in_play(s), s.myturn, s.guess, s.turn_state)
end



