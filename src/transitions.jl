
type DudoGuessDistribution
    count::Tuple{Int,Int}
    val::Tuple{Int,Int}
    must_raise::Bool
    exact::Bool
    exact_guess::DudoGuess
end
random_guess_distribution(n_sides_die, dice_in_play) = DudoGuessDistribution((1,dice_in_play), (1, n_sides_die), false, false, no_guess)

function n_guesses(d::DudoGuessDistribution)
	if d.exact
		return 1
	end
	n_guesses = length(collect(product(d.count[1]:d.count[2], d.val[1]:d.val[2])))
	if d.must_raise
		n_guesses -= 1
	end
	n_guesses
end

function is_in_distribution(d::DudoGuessDistribution, g::DudoGuess)
	if d.exact
		return d.exact_guess.count == g.count && d.exact_guess.val == g.val
	end
	if d.must_raise
		if g.count == d.count[1] && g.val == d.val[1]
			return false
		end
	end
	return g.count in d.count[1]:d.count[2] && g.val in d.val[1]:d.val[2]
end

function exact_guess_distribution(guess::DudoGuess)
	DudoGuessDistribution((guess.count, guess.count), (guess.val, guess.val), true, true, guess)
end

function atleast_guess_distribution(minCount::Int, maxCount::Int, minVal::Int, maxVal::Int)
	DudoGuessDistribution((minCount, maxCount), (minVal, maxVal), true, false, no_guess)
end

function pdf(d::DudoGuessDistribution, g::DudoGuess)
	if !is_in_distribution(d, g)
		return 0
	end
	return 1 / n_guesses(d)
end
function Base.rand(d::DudoGuessDistribution)
	if d.exact
		return d.exact_guess
	end
	count = rand(d.count[1]:d.count[2])
	val = rand(d.val[1]:d.val[2])
	g = DudoGuess(count, val)
	if d.must_raise
		while g.count == d.count[1] && g.val == d.val[1]
			g.count = rand(d.count[1]:d.count[2])
			g.val = rand(d.val[1]:d.val[2])
		end
	end
	g
end

type DudoHandDistribution
	dice::Vector{Distribution}
	DudoHandDistribution(n_sides_die::Int, hand_size::Int) = new([DiscreteUniform(1, n_sides_die) for i in 1:hand_size])
	DudoHandDistribution(hand::Vector{Int}) = new([DiscreteUniform(die, die) for die in sort(hand)])
end
function is_exact_hand(d::DudoHandDistribution)
	for dieDistribution in d.dice
		if dieDistribution.a != dieDistribution.b
			return false
		end
	end
	true
end

random_hand_distribution(n_sides_die::Int, hand_size::Int) = DudoHandDistribution(n_sides_die, hand_size)
exact_hand_distribution(hand::Vector{Int}) = DudoHandDistribution(hand)

function pdf(d::DudoHandDistribution, h::Vector{Int})
	if length(d.dice) == 0
		return length(h) == 0 ? 1 : 0
	elseif length(h) == 0
		return 0
	elseif length(d.dice) != length(h)
		return 0
	end
	pdf = 1
	for i in 1:length(sort(h))
		die = h[i]
		pdf *= Distributions.pdf(d.dice[i], die)
	end
	if !is_exact_hand(d)
		pdf *= factorial(length(Set(h)))
	end
	pdf
end
function Base.rand(d::DudoHandDistribution)
	[rand(die) for die in d.dice]
end

type DudoTransitionDistribution <: AbstractDistribution
    my_hand_distribution::DudoHandDistribution
    other_hand_distribution::DudoHandDistribution
    myturn::Bool
    guess_distribution::DudoGuessDistribution
    turn_state_distribution::Distribution
end
function POMDPs.create_transition_distribution(pomdp::DudoPomdp)
	my_hand_distribution = random_hand_distribution(pomdp.n_sides_die, pomdp.starting_hand_size)
	other_hand_distribution = random_hand_distribution(pomdp.n_sides_die, pomdp.starting_hand_size)
	myturn = true
	guess_distribution = random_guess_distribution(pomdp.n_sides_die, max_dice(pomdp))
	turn_state_distribution = exact_turn_state_distribution(new_round)
	DudoTransitionDistribution(my_hand_distribution, other_hand_distribution, myturn, guess_distribution, turn_state_distribution)
end

function exact_turn_state_distribution(exact::TurnState)
	DiscreteUniform(index(exact), index(exact))
end
function uniform_turn_state_distribution()
	DiscreteUniform(1, 2) # other player can only raise or challenge.
end


# For the current distribution, what's the probability density of this state?
function POMDPs.pdf(d::DudoTransitionDistribution, s::DudoState)
	if d.myturn != s.myturn
		return 0
	end
	pdf = 1
	pdf *= Dudo.pdf(d.my_hand_distribution, s.my_hand)
	pdf *= Dudo.pdf(d.other_hand_distribution, s.other_hand)
	pdf *= Distributions.pdf(d.turn_state_distribution, index(s.turn_state))
	if s.turn_state == challenged
		# A challenge must have the same guess as the previous raise, so it's the min from guess
		if d.guess_distribution.exact
			# It's the min.
			if s.guess.count == d.guess_distribution.exact_guess.count && s.guess.val == d.guess_distribution.exact_guess.val
				pdf *= 1
			else
				pdf *= 0
			end
		else
			# It's the min.
			if s.guess.count == d.guess_distribution.count[1] && s.guess.val == d.guess_distribution.val[1]
				pdf *= 1
			else
				pdf *= 0
			end
		end
	else
		pdf *= Dudo.pdf(d.guess_distribution, s.guess)
	end
	pdf
end

# Sample from transition distribution
function POMDPs.rand!(rng::AbstractRNG, s::DudoState, d::DudoTransitionDistribution)
	s.my_hand = rand(d.my_hand_distribution)
	s.other_hand = rand(d.other_hand_distribution)
    s.myturn = d.myturn
    s.turn_state = turn_state_from_index(rand(d.turn_state_distribution))
    if s.turn_state == raised
    	s.guess = rand(d.guess_distribution)
    elseif s.turn_state == challenged
    	s.guess = DudoGuess(d.guess_distribution.count[1], d.guess_distribution.val[1])
    else
    	# TODO ?
    	s.guess = no_guess
    end
    return s
end

### STATE DEFINITION
	# my_hand::Vector{Int}
	# other_hand::Vector{Int}
	# #counts::Vector{Int64}
	# myturn::Bool
	# guess::DudoGuess
	# turn_state::TurnState

# TODO: clean this up.
# For the current (state, action), update the transition distribution
function POMDPs.transition(pomdp::DudoPomdp, s::DudoState, a::DudoAction, d::DudoTransitionDistribution=create_transition_distribution(pomdp))
	if is_game_over(s)
		update(d, game_over_distribution(pomdp))
		return d
	end
	if is_max_guess(s.guess, dice_in_play(pomdp, s), pomdp)
		if s.myturn
			if a.action != challenge
				update(d, distribution_from_state(s))
				return d
			end
		else
			# Other player must challenge
			d.my_hand_distribution = exact_hand_distribution(s.my_hand)
			d.other_hand_distribution = exact_hand_distribution(s.other_hand)
			d.turn_state_distribution = exact_turn_state_distribution(challenged)
			d.guess_distribution = exact_guess_distribution(s.guess)
			d.myturn = !s.myturn
			return d
		end
	end
	if s.myturn
		if s.turn_state == raised
			if a.action == raise
				if ((a.raise.val < s.guess.val || a.raise.count < s.guess.count)
						|| (a.raise.val == s.guess.val && a.raise.count == s.guess.count))
					# Raising to the same value should do nothing.
					update(d, distribution_from_state(s))
					return d
				elseif (a.raise.count > dice_in_play(pomdp, s))
					# Raising to an invalid guess does nothing.
					update(d, distribution_from_state(s))
					return d
				end
				# Otherwise, take the raise action
				d.my_hand_distribution = exact_hand_distribution(s.my_hand)
				d.other_hand_distribution = exact_hand_distribution(s.other_hand)
				d.guess_distribution = exact_guess_distribution(a.raise)
				d.turn_state_distribution = exact_turn_state_distribution(raised)
			elseif a.action == challenge
				if is_no_guess(s.guess)
#					println("Not a valid challenge")
					update(d, distribution_from_state(s))
					return d
				end
				# TODO: who starts next round after a challenge? For now, loser.
				if check_guess(pomdp, s.guess, s)
					# My challenge failed, so I lose a die.
					update(d, new_round_distribution(pomdp.n_sides_die, length(s.my_hand) - 1, length(s.other_hand), true))
					return d
				else
					# Other player loses a die.
					update(d, new_round_distribution(pomdp.n_sides_die, length(s.my_hand), length(s.other_hand) - 1, false))
					return d
				end
			else # action is not raise or challenge, so do nothing.
				update(d, distribution_from_state(s))
				return d
			end
		elseif s.turn_state == challenged
			if a.action != end_round
				# Only action should be end_round.
#				println("I was challenged and didn't end round")
				update(d, distribution_from_state(s))
				return d
			elseif is_no_guess(s.guess)
#				println("Not a valid challenge")
				update(d, distribution_from_state(s))
				return d
			else
#				println("I was challenged, end_round")
				if check_guess(pomdp, s.guess, s)
					# Their challenge failed.
#					println("They lose die")
					dn = new_round_distribution(pomdp.n_sides_die, length(s.my_hand), length(s.other_hand) - 1, false)
					update(d, dn)
					return d
				else
#					println("I lose die")
					# Their challenge succeeded, so I lose a die.
					update(d, new_round_distribution(pomdp.n_sides_die, length(s.my_hand) - 1, length(s.other_hand), true))
					return d
				end
			end
		elseif s.turn_state == new_round
			if a.action == raise
				# TODO same as above
				if ((a.raise.val < s.guess.val || a.raise.count < s.guess.count)
						|| (a.raise.val == s.guess.val && a.raise.count == s.guess.count))
					# Raising to the same value should do nothing.
					update(d, distribution_from_state(s))
					return d
				elseif (a.raise.count > dice_in_play(pomdp, s))
#					println("Invalid guess")
					# Raising to an invalid guess does nothing.
					update(d, distribution_from_state(s))
					return d
				end
				# Otherwise, take the raise action
				d.my_hand_distribution = exact_hand_distribution(s.my_hand)
				d.other_hand_distribution = exact_hand_distribution(s.other_hand)
				d.guess_distribution = exact_guess_distribution(a.raise)
				d.turn_state_distribution = exact_turn_state_distribution(raised)
			else # This is start of new round, so raise should be only valid action.
				# Do nothing.
				update(d, distribution_from_state(s))
				return d
			end
		end
	else
		# Not my turn.
		# s.turn_state = my last action (raised, challenged, or new_round)
		# a = my new action, all of which should have same effect (wait)
		# TODO: i think all s.actions are the same.
		if a.action != wait
#			println("Not my turn, should just wait.")
			update(d, distribution_from_state(s))
			return d
		end
		d.my_hand_distribution = exact_hand_distribution(s.my_hand)
		d.other_hand_distribution = exact_hand_distribution(s.other_hand)
		if s.turn_state == new_round || s.turn_state == challenged
			# Other player must raise (start of new round)
			d.turn_state_distribution = exact_turn_state_distribution(raised)
			d.guess_distribution = random_guess_distribution(pomdp.n_sides_die, dice_in_play(pomdp, s))
		else
			# Other player must raise or challenge.
			d.turn_state_distribution = uniform_turn_state_distribution()
			if s.guess.val == 0 || s.guess.count == 0
				d.guess_distribution = random_guess_distribution(pomdp.n_sides_die, dice_in_play(pomdp, s))
			else
				d.guess_distribution = atleast_guess_distribution(s.guess.count, dice_in_play(pomdp, s), s.guess.val, pomdp.n_sides_die)
			end
		end
	end
	d.myturn = !s.myturn
	d
end
function update(d, dn)
	d.my_hand_distribution = dn.my_hand_distribution
	d.other_hand_distribution = dn.other_hand_distribution
	d.myturn = dn.myturn
	d.guess_distribution = dn.guess_distribution
	d.turn_state_distribution = dn.turn_state_distribution
end

function new_round_distribution(n_sides_die, my_hand_size, other_hand_size, myturn)
	my_hand_distribution = my_hand_size == 0 ? exact_hand_distribution(Int[]) : random_hand_distribution(n_sides_die, my_hand_size)
	other_hand_distribution = other_hand_size == 0 ? exact_hand_distribution(Int[]) : random_hand_distribution(n_sides_die, other_hand_size)
	d = DudoTransitionDistribution(my_hand_distribution, other_hand_distribution, myturn, exact_guess_distribution(no_guess), exact_turn_state_distribution(new_round))
	d
end

function distribution_from_state(s::DudoState)
	my_hand_distribution = exact_hand_distribution(s.my_hand)
	other_hand_distribution = exact_hand_distribution(s.other_hand)
	myturn = s.myturn
	guess_distribution = exact_guess_distribution(s.guess)
	turn_state_distribution = exact_turn_state_distribution(s.turn_state)
	d = DudoTransitionDistribution(my_hand_distribution, other_hand_distribution, myturn, guess_distribution, turn_state_distribution)
	d
end

function game_over_distribution(p::DudoPomdp)
	my_hand_distribution = exact_hand_distribution(Int[])
	other_hand_distribution = exact_hand_distribution(Int[])
	myturn = false
	guess_distribution = exact_guess_distribution(no_guess)
	turn_state_distribution = exact_turn_state_distribution(new_round)
	DudoTransitionDistribution(my_hand_distribution, other_hand_distribution, myturn, guess_distribution, turn_state_distribution)
end

function index(turn_state::TurnState)
	if turn_state == raised
		return 1
	elseif turn_state == challenged
		return 2
	elseif turn_state == new_round
		return 3
	end
end

function turn_state_from_index(prev::Int)
	if prev == 1
		return raised
	elseif prev == 2
		return challenged
	elseif prev == 3
		return new_round
	end
end
