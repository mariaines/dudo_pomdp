
type DudoPomdp <: POMDP
	n_sides_die::Int # n-sided die
	starting_hand_size::Int # starting hand size
	r_win_challenge::Float64
	r_lose_challenge::Float64
	r_win_game::Float64
	r_lose_game::Float64
	r_forbidden_action::Float64
	discount_factor::Float64
	DudoPomdp(n, h) = new(n,h, 10, -10, 100, -100, -20, 0.9)
end
max_dice(pomdp::DudoPomdp) = 2 * pomdp.starting_hand_size
DudoPomdp() = DudoPomdp(2, 1)

function Base.show(io::Main.Base.IO, d::DudoPomdp)
	print(io, "DudoPomdp: {")
	print(io, "$(d.n_sides_die)-sided die, ")
	print(io, "starting_hand_size: $(d.starting_hand_size)")
	print(io, "}")
end

POMDPs.discount(pomdp::DudoPomdp) = pomdp.discount_factor


function make_pomdpx(pomdp::DudoPomdp)
	dir = "$(pwd())/pomdpx"
	if !isdir(dir)
		mkdir(dir)
	end
	filename = "$(dir)/dudo_$(pomdp.n_sides_die)_$(pomdp.starting_hand_size).pomdpx"
	pomdpx = POMDPX(filename, description=string(pomdp))
	if !isfile(filename)
		POMDPXFile.write(pomdp, pomdpx)
	else
		println("Loading pre-existing pomdpx file: $filename")
	end
	pomdpx
end