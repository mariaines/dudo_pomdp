abstract Opponent
choose_action(o::Opponent, pomdp::DudoPomdp, obs::DudoObservation) = error("choose_action not implemented")

function get_next_state(o::Opponent, pomdp::DudoPomdp, obs::DudoObservation, d::DudoTransitionDistribution)
	@test is_opponents_turn(obs)

	turn_state
	action = choose_action(o, pomdp, obs)
	if action.action == raise
		turn_state = raised
	else
		turn_state = challenged
	end


	# hand gets filled automatically
	s = create_state(pomdp)
	rand!(MersenneTwister(), s, d)
	if typeof(o) != HumanOpponent
		# @show obs
		# @show s
	end

	if obs.turn_state == new_round || obs.turn_state == challenged
		@test turn_state == raised
	else
		#@test turn_state != new_round
	end

	s.turn_state = turn_state
	if turn_state == raised
		@test is_valid_raise(action.raise, obs.guess, obs.dice_in_play)
		s.guess = action.raise
	else
		s.guess = obs.guess
	end
	s
end

is_opponents_turn(obs::DudoObservation) = !obs.myturn
function is_game_over(obs::DudoObservation)
	return length(obs.my_hand) == 0 || dice_in_play - length(obs.my_hand) == 0
end

## Random opponent, default.
type RandomOpponent <: Opponent
	rng::MersenneTwister
end
RandomOpponent() = RandomOpponent(MersenneTwister())

function choose_action(o::RandomOpponent, pomdp::DudoPomdp, obs::DudoObservation)
	if obs.turn_state == new_round
		t = raise
	else
		if is_max_guess(obs.guess, obs.dice_in_play, pomdp)
			t = challenge
		else
			t = opponent_action_from_index(rand(1:2))
		end
	end
	# @show t
	if t == challenge
		return DudoAction(t)
	end
	guess_distribution
	if obs.guess.val == 0 || obs.guess.count == 0
		guess_distribution = random_guess_distribution(pomdp.n_sides_die, obs.dice_in_play)
	else
		guess_distribution = atleast_guess_distribution(obs.guess.count, obs.dice_in_play, obs.guess.val, pomdp.n_sides_die)
	end
	guess = rand(guess_distribution)
	DudoAction(t, guess)
end


function opponent_action_from_index(i::Int)
	if i == 1
		return raise
	elseif i == 2
		return challenge
	end
	error("Not a valid opponent action index $i")
end



type NaiveOpponent <: Opponent
end

function choose_action(o::NaiveOpponent, pomdp::DudoPomdp, obs::DudoObservation)
	t
	if obs.turn_state == new_round || obs.turn_state == challenged
		t = raise
	else
		if is_max_guess(obs.guess, obs.dice_in_play, pomdp)
			t = challenge
		elseif obs.guess.count/obs.dice_in_play > 1/pomdp.n_sides_die
			t = challenge
		else
			t = raise
		end
	end
	if t == challenge
		return DudoAction(t)
	end
	guess

	# Can't guess less than 1
	guessVal = obs.guess.val < 1 ? 1 : obs.guess.val
	# Now choose the raise with lowest possible risk
	if ((obs.guess.count + 1)/obs.dice_in_play <= 1/pomdp.n_sides_die
			# at the max value, so have to raise the count.
			|| guessVal == pomdp.n_sides_die)
		guess = DudoGuess(obs.guess.count + 1, guessVal)
	else
		guessCount = obs.guess.count < 1 ? 1 : obs.guess.count
		guess = DudoGuess(guessCount, guessVal + 1)
	end
	DudoAction(t, guess)
end



function input(prompt::String="")
	println(prompt)
	print(">> ")
	chomp(readline())
end

type HumanOpponent <: Opponent
end

function choose_action(o::HumanOpponent, pomdp::DudoPomdp, obs::DudoObservation)
	println("\nYour hand: $(obs.my_hand)")
	println("Dice in play: $(obs.dice_in_play)")
	println("Current guess: $(obs.guess) ($(obs.turn_state))\n")

	t
	if is_max_guess(obs.guess, obs.dice_in_play, pomdp)
		action = input("At max guess, you must challenge. (hit enter to continue)")
		t = challenge
	elseif obs.turn_state == new_round || obs.turn_state == challenged
		println("New round, you must raise.")
		t = raise
	else
		action = input("Raise or challenge? (type 'raise' or 'challenge')")
		while action != "raise" && action != "challenge"
			println("Didn't recognize command $action.")
			action = input("Raise or challenge? (type 'raise' or 'challenge')")
		end
		if action == "raise"
			t = raise
		elseif action == "challenge"
			t = challenge
		else
			error("Invalid action $action")
		end
	end

	if t == challenge
		return DudoAction(t)
	end

	count
	val
	if obs.guess.val == pomdp.n_sides_die
		println("At highest value, so value must be $(obs.guess.val) and you must increase count")
		count = get_int(obs.guess.count + 1, obs.dice_in_play, "Enter count for guess (for six threes, type '6')")
		val = obs.guess.val
	else
		count = get_int(obs.guess.count, obs.dice_in_play, "Enter count for guess (for six threes, type '6')")
		minVal = count == obs.guess.count ? obs.guess.val + 1 : obs.guess.val
		val = get_int(minVal, pomdp.n_sides_die, "Enter value for guess (for six threes, type '3')")
	end

	guess = DudoGuess(count, val)
	DudoAction(t, guess)
end

function get_int(min::Int, max::Int, prompt::String="")
	prompt = string(prompt, " ($min:$max)")
	qInput = input(prompt)
	qNullable = tryparse(Int, qInput)
	while isnull(qNullable) || get(qNullable) < min || get(qNullable) > max
		if isnull(qNullable)
			println("Didn't recognize number $qInput.")
		else
			println("Number not in range ($min:$max)")
		end
		qInput = input(prompt)
		qNullable = tryparse(Int, qInput)
	end
	get(qNullable)
end


function get_char(letters::String, prompt::String="")
	prompt = string(prompt, " (one of [$letters])")
	letterInput = input(prompt)
	while isempty(letterInput) || !(letterInput[1] in letters)
		if isempty(letterInput)
			println("Must enter one of [$letters]")
		else
			println("Input $letterInput not in [$letters]")
		end
		letterInput = input(prompt)
	end
	letterInput[1]
end




