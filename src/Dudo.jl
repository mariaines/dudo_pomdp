module Dudo

using POMDPs, SARSOP, QMDP, POMDPXFile, POMDPToolbox#, POMDPDistributions
using Iterators, Distributions, Base.Test

import Base.show

export
	# pomdp.jl
	DudoPomdp,
	make_pomdpx,

	# opponents.jl
	Opponent, # Abstract type
	RandomOpponent,
	NaiveOpponent,
	HumanOpponent,


	# simulator.jl
	simulate,
	play

include("pomdp.jl")
include("states.jl")
include("actions.jl")
include("transitions.jl")
include("observations.jl")
include("rewards.jl")
include("beliefs.jl")
include("opponents.jl")
include("simulator.jl")

end #module