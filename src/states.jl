
type DudoGuess
	count::Int64
	val::Int64
end
function Base.show(io::Main.Base.IO, d::DudoGuess)
	print(io, "DudoGuess: ($(d.count),$(d.val))")
end
no_guess = DudoGuess(0,0)
is_no_guess(g::DudoGuess) = g.count == 0 && g.val == 0
is_max_guess(g::DudoGuess, dice_in_play::Int, pomdp::DudoPomdp) = g.count == dice_in_play && g.val == pomdp.n_sides_die
@enum TurnState raised challenged new_round

type DudoState <: State
	my_hand::Vector{Int}
	other_hand::Vector{Int}
	myturn::Bool
	guess::DudoGuess
	turn_state::TurnState
end
DudoState(hand1, hand2) = DudoState(hand1, hand2, rand([true,false]), no_guess, new_round)

function Base.show(io::Main.Base.IO, d::DudoState)
	print(io, "DudoState: {")
	print(io, "$(d.my_hand), ")
	print(io, "$(d.other_hand), ")
	print(io, "myturn: $(d.myturn), ")
	print(io, "$(d.guess), ")
	print(io, "$(d.turn_state)")
	print(io, "}")
end
function is_game_over(s::DudoState)
	return length(s.my_hand) == 0 || length(s.other_hand) == 0
end
function is_terminal(s::DudoState)
	return length(s.my_hand) == 0 && length(s.other_hand) == 0
end
function counts(p::DudoPomdp, s::DudoState)
	counts = zeros(p.n_sides_die)
	for die in s.my_hand
		counts[die] += 1
	end
	for die in s.other_hand
		counts[die] += 1
	end
	counts
end
dice_in_play(p::DudoPomdp, s::DudoState) = Int(sum(counts(p, s)))
dice_in_play(s::DudoState) = length(s.my_hand) + length(s.other_hand)

type DudoStateSpace <: AbstractSpace
	states::Vector{DudoState}
end

# Initial state
function POMDPs.create_state(pomdp::DudoPomdp)
	hand1 = [rand(1:pomdp.n_sides_die) for i in 1:pomdp.starting_hand_size]
	hand2 = [rand(1:pomdp.n_sides_die) for i in 1:pomdp.starting_hand_size]
	DudoState(hand1, hand2)
end

POMDPs.domain(space::DudoStateSpace) = space.states;

function POMDPs.states(pomdp::DudoPomdp)
	states = []
	for hand1_size in 0:pomdp.starting_hand_size, hand2_size in 0:pomdp.starting_hand_size
		if hand1_size == 0 && hand2_size == 0
			hand1 = Int[]
			hand2 = Int[]
			# Only 1, this is game over
			push!(states, DudoState(hand1, hand2, false, no_guess, new_round))
			continue
		end
		for hand1 in enumerate_hands(pomdp.n_sides_die, hand1_size), hand2 in enumerate_hands(pomdp.n_sides_die, hand2_size)
			#hand1 = Int[i for i in hand1]
			#hand2 = Int[i for i in hand2]
			# Special cases if one hand is zero: someone just lost a die, and next state will
			# be game over. 
			if hand1_size == 0
				hand1 = Int[i for i in hand1]
				# I just lost a die, new_round, my turn
				push!(states, DudoState(hand1, hand2, true, no_guess, new_round))
			elseif hand2_size == 0
				hand2 = Int[i for i in hand2]
				# They just lost a die, new_round, not my turn
				# TODO who starts after losing a die? For now, loser.
				push!(states, DudoState(hand1, hand2, false, no_guess, new_round))
			else
				for myturn in [true, false]
					push!(states, DudoState(hand1, hand2, myturn, no_guess, new_round))
					for guess in enumerate_guesses(hand1_size + hand2_size, pomdp.n_sides_die)
						push!(states, DudoState(hand1, hand2, myturn, guess, raised))
						if myturn
							# We are only in challenged if it's my turn.
							push!(states, DudoState(hand1, hand2, myturn, guess, challenged))
						end
					end
				end
				# TODO is this right?? A round can end at (my_turn, challenged) or (not_my_turn, new_round).
				# push!(states, DudoState(hand1, hand2, false, no_guess, new_round))
			end
		end
	end
	DudoStateSpace(states)
end

function POMDPs.n_states(pomdp::DudoPomdp)
	# TODO calculate this better
	length(states(pomdp).states)
	# OLD
	# max_count = pomdp.nplayers * pomdp.starting_hand_size
	# n_states = 0
	# for dice_in_play in 1:max_count
	# 	n = dice_in_play
	# 	m = pomdp.n_sides_die
	# 	# actual state * possible guesses * roundInPlay (T/F)
	# 	n_states += n_balls_to_m_buckets(n,m) * n_guesses(pomdp, dice_in_play) * 3
	# end
	# n_states
end
n_balls_to_m_buckets(n,m) = factorial(n+m-1) / (factorial(n)factorial(m-1))

function enumerate_hands(n_sides_die::Int, handsize::Int)
	if handsize == 0
		return [()]
	end
 	#sort([sort(Int[i for i in tuple]) for tuple in collect(product(repeated(1:n_sides_die, handsize)...))], by=sum)
	hands = collect(Set([sort(Int[i for i in tuple]) for tuple in collect(product(repeated(1:n_sides_die, handsize)...))]))
	# Sort in order of each die
	for i in reverse(1:handsize)
       sort!(hands, by=hand->hand[i])
    end
    hands
end

function enumerate_guesses(n_dice::Int, n_sides_die::Int)
	guesses = []
  	for count in 1:n_dice  # count
  		for val in 1:n_sides_die
  			push!(guesses, DudoGuess(count, val))
  		end
  	end
  	guesses
end

# True if the guess is valid, false otherwise
function check_guess(pomdp::DudoPomdp, guess::DudoGuess, state::DudoState)
	if guess.val == 0
		return false
	end
	guess.count <= counts(pomdp, state)[guess.val]
end



