type DudoObservation <: Observation
	my_hand::Vector{Int}
	dice_in_play::Int
	myturn::Bool
	guess::DudoGuess
	turn_state::TurnState
end
POMDPs.create_observation(pomdp::DudoPomdp) = DudoObservation([], max_dice(pomdp), true, no_guess, new_round)
POMDPs.create_observation(pomdp::DudoPomdp, s::DudoState) = DudoObservation(s.my_hand, max_dice(pomdp), true, no_guess, new_round)

POMDPs.n_observations(pomdp::DudoPomdp) = length(POMDPs.observations(pomdp).obs)

type DudoObservationSpace <: AbstractSpace
    obs::Vector{DudoObservation} 
end

POMDPs.domain(space::DudoObservationSpace) = space.obs;

function POMDPs.observations(pomdp::DudoPomdp)
	observations = []
	for hand1_size in 0:pomdp.starting_hand_size
		if hand1_size == 0
			hand1 = Int[]
			# If I have zero, then max is starting hand of other player
			for dice_in_play in 0:pomdp.starting_hand_size
				for myturn in [true, false]
				 	push!(observations, DudoObservation(hand1, dice_in_play, myturn, no_guess, new_round))
				end
			end
			continue
		end
		for hand1 in enumerate_hands(pomdp.n_sides_die, hand1_size)
			hand1 = Int[i for i in hand1]
			for dice_in_play in 1:pomdp.starting_hand_size + hand1_size
				for myturn in [true, false]
					for guess in enumerate_guesses(dice_in_play, pomdp.n_sides_die)
						push!(observations, DudoObservation(hand1, dice_in_play, myturn, guess, raised))
						push!(observations, DudoObservation(hand1, dice_in_play, myturn, guess, challenged))
					end
					push!(observations, DudoObservation(hand1, dice_in_play, myturn, no_guess, new_round))
					# TODO: maybe remove this?
					push!(observations, DudoObservation(hand1, dice_in_play, myturn, no_guess, challenged))
				end
			end
		end
	end
	DudoObservationSpace(observations)
end

# SARSOP requires all (s,a) pairs, so just return full observation space.
# POMDPs.observations(pomdp::DudoPomdp, s::DudoState, obs::DudoObservationSpace)
function POMDPs.observations(pomdp::DudoPomdp, s::DudoState, obs::DudoObservationSpace)
	# f = filter(filter_obs_space, obs.obs)
	# println("Filtered to $(length(f))/$(length(obs.obs)) observations")
	# DudoObservationSpace(f)
	obs
end
function filter_obs_space(o)
	if o.my_hand != s.my_hand
		return false
	elseif o.dice_in_play != dice_in_play(pomdp, s)
		return false
	elseif o.myturn != s.myturn
		return false
	elseif o.guess != s.guess
		return false
	elseif o.turn_state != s.turn_state
		return false
	end
	true
end

# Observation distribution type
type DudoObservationDistribution <: AbstractDistribution
    my_hand_distribution::DudoHandDistribution
    dice_in_play::Int
    myturn::Bool
    guess_distribution::DudoGuessDistribution
    turn_state_distribution::Distribution
end

function Base.show(io::Main.Base.IO, d::DudoGuessDistribution)
	print(io, "DudoGuessDistribution: {")
	if d.exact
		print(io, "exact: $(d.exact_guess), ")
	else
		print(io, "inexact: ($(d.count[1]):$(d.count[2]), $(d.val[1]):$(d.val[2])), ")
	end
	print(io, "must_raise: $(d.must_raise)")
	print(io, "}")
end
function Base.show(io::Main.Base.IO, d::DudoHandDistribution)
	print(io, "[")
	for i in 1:length(d.dice)
		die = d.dice[i]
		if die.a == die.b
			print(io, die.a)
		else
			print(io, "$(die.a):$(die.b)")
		end
		if i < length(d.dice)
			print(io, ",")
		end
	end
	print(io, "]")
end
function Base.show(io::Main.Base.IO, d::DudoObservationDistribution)
	print(io, "DudoObservationDistribution: {\n")
	print(io, "my_hand: $(d.my_hand_distribution), ")
	print(io, "dice_in_play: $(d.dice_in_play), ")
	print(io, "myturn: $(d.myturn), ")
	print(io, "\nguess: $(d.guess_distribution),\n")
	if d.turn_state_distribution.a == d.turn_state_distribution.b
		exact_action = turn_state_from_index(d.turn_state_distribution.a)
		print(io, "turn_state: $exact_action")
	else
		print(io, "turn_state: ")
		for i in d.turn_state_distribution.a:d.turn_state_distribution.b
			print(io, turn_state_from_index(i), ",")
		end
	end
	print(io, "}\n")
end

function POMDPs.create_observation_distribution(pomdp::DudoPomdp)
	my_hand_distribution = random_hand_distribution(pomdp.n_sides_die, pomdp.starting_hand_size)
	dice_in_play = max_dice(pomdp)
	myturn = true
	guess_distribution = random_guess_distribution(pomdp.n_sides_die, max_dice(pomdp))
	turn_state_distribution = exact_turn_state_distribution(new_round)
	DudoObservationDistribution(my_hand_distribution, dice_in_play, myturn, guess_distribution, turn_state_distribution)
end

# observation pdf
function POMDPs.pdf(d::DudoObservationDistribution, o::DudoObservation)
	if d.myturn != o.myturn
		return 0
	elseif d.dice_in_play != o.dice_in_play
		return 0
	end
	pdf = 1
	pdf *= Dudo.pdf(d.my_hand_distribution, o.my_hand)
	pdf *= Dudo.pdf(d.guess_distribution, o.guess)
	pdf *= Distributions.pdf(d.turn_state_distribution, index(o.turn_state))
	pdf
end

# Observation function. We just observe everything from the state except for 
# the other player's hand. Action doesn't matter.
function POMDPs.observation(pomdp::DudoPomdp, s::DudoState, a::DudoAction, 
		d::DudoObservationDistribution=create_observation_distribution(pomdp))
	d.my_hand_distribution = exact_hand_distribution(s.my_hand)
	d.dice_in_play = dice_in_play(pomdp, s)
	d.myturn = s.myturn
	d.turn_state_distribution = exact_turn_state_distribution(s.turn_state)
	d.guess_distribution = exact_guess_distribution(s.guess)
	d
end

# Sample from the observation distribution
function POMDPs.rand!(rng::AbstractRNG, o::DudoObservation, d::DudoObservationDistribution)
    o.my_hand = rand(d.my_hand_distribution)
    o.dice_in_play = d.dice_in_play
    o.myturn = d.myturn
    o.turn_state = turn_state_from_index(rand(d.turn_state_distribution))
    if o.turn_state == raised
    	o.guess = rand(d.guess_distribution)
    elseif o.turn_state == challenged
    	o.guess = DudoGuess(d.guess_distribution.count[1], d.guess_distribution.val[1])
    else
    	o.guess = no_guess
    end
    return o
end

function enumerate_hands(pomdp::DudoPomdp, dice_in_play::Int)
	n_sides_die = pomdp.n_sides_die
	hands = Set()
	starting_hand_size  = min(pomdp.starting_hand_size, fld(dice_in_play, nplayers))
	for p in product(repeated(1:n_sides_die, starting_hand_size)...)
		hand = [p...]
		push!(hands, hand)
	end
	collect(hands)
end

