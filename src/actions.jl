
@enum DudoActionType raise challenge wait end_round

type DudoAction <: Action
	action::DudoActionType
	raise::DudoGuess
end
DudoAction(a::DudoActionType) = DudoAction(a, no_guess)

type DudoActionSpace <: AbstractSpace
	actions::Vector{DudoAction}
end

POMDPs.domain(space::DudoActionSpace) = space.actions;

function POMDPs.actions(pomdp::DudoPomdp)
	max_dice_in_play = max_dice(pomdp)
	actions = []
	for guess in enumerate_guesses(max_dice_in_play, pomdp.n_sides_die)
		push!(actions, DudoAction(raise, guess))
	end
	push!(actions, DudoAction(challenge))
	push!(actions, DudoAction(wait))
	push!(actions, DudoAction(end_round))
	DudoActionSpace(actions)
end

# SARSOP requires all (s,a) pairs, so just return full action space.
# POMDPs.actions(pomdp::DudoPomdp, state::DudoState, acts::DudoActionSpace) = acts
function POMDPs.actions(pomdp::DudoPomdp, state::DudoState, acts::DudoActionSpace)
	# filtered = []
	# actions = acts.actions
	# if state.myturn
	# 	if state.turn_state == challenged
	# 		filtered = filter(a -> a.action == end_round, actions)
	# 	elseif state.turn_state == raised
	# 		filtered = filter(
	# 			function(a)
	# 				if a.action == challenge
	# 					return true
	# 				elseif a.action != raise # Can't end round or wait.
	# 					return false
	# 				end
	# 				is_valid_raise(a.raise, state.guess, dice_in_play(pomdp, state))
	# 			end, actions)
	# 	else # New round
	# 		filtered = filter(a -> a.action == raise, actions)
	# 	end
	# else
	# 	filtered = filter(a -> a.action == wait, actions)
	# end
	# println("Filtered to $(length(filtered))/$(length(actions)) actions")
	# DudoActionSpace(filtered)
	#acts.actions = filtered
	acts
end

function is_valid_raise(raise::DudoGuess, previousGuess::DudoGuess, dice_in_play::Int)
	if raise.val < previousGuess.val || raise.count < previousGuess.count
		# Can't lower val or count.
		return false
	elseif raise.val == previousGuess.val && raise.count == previousGuess.count
		# Can't raise to the same value.
		return false
	elseif raise.count > dice_in_play
		# Can't guess higher than the number of dice in play.
		return false
	end
	true
end

function POMDPs.n_actions(pomdp::DudoPomdp)
	length(actions(pomdp).actions)
	# TODO calculate this better
	# max_dice_in_play = pomdp.nplayers * pomdp.starting_hand_size
	# n_actions = n_guesses(pomdp, max_dice_in_play) * 2
	# n_actions
end