function POMDPs.reward(pomdp::DudoPomdp, s::DudoState, a::DudoAction)
    if is_game_over(s)
        return r_game_over(pomdp, s)
    end
    r = 0
    # TODO clean this up.
    if s.myturn
        if a.action == wait
            return pomdp.r_forbidden_action
        end
        if s.turn_state == raised
            if a.action == raise
                # Raise has to actually raise the value
                if ((a.raise.val < s.guess.val || a.raise.count < s.guess.count)
                        || (a.raise.val == s.guess.val && a.raise.count == s.guess.count))
                # if a.raise.val <= s.guess.val && a.raise.count <= s.guess.count
                    return pomdp.r_forbidden_action
                elseif a.raise.val > pomdp.n_sides_die || a.raise.count > dice_in_play(s)
                    return pomdp.r_forbidden_action
                end
                return 0
            elseif a.action == challenge
                return r_challenge(pomdp, s.guess, s, true)
            elseif a.action == end_round || a.action == wait
                return pomdp.r_forbidden_action
            end
        elseif s.turn_state == challenged
            # TODO
            if a.action != end_round
                return pomdp.r_forbidden_action
            end
            return r_challenge(pomdp, s.guess, s, false)
        elseif s.turn_state == new_round
            if a.action != raise
                return pomdp.r_forbidden_action
            end
            if a.raise.val > pomdp.n_sides_die || a.raise.count > dice_in_play(s)
                return pomdp.r_forbidden_action
            elseif a.raise.val == 0 && a.raise.count == 0
                return pomdp.r_forbidden_action
            end
            if is_no_guess(s.guess)
                return 0
            end
            if a.raise.val <= s.guess.val && a.raise.count <= s.guess.count
                return pomdp.r_forbidden_action
            end
            return 0
        end
    else
        # Not my turn, everything results in "wait"
        if a.action != wait
            return pomdp.r_forbidden_action
        else 
            return 0
        end
    end
    r
end


function r_challenge(pomdp::DudoPomdp, guess::DudoGuess, state::DudoState, challenger::Bool)
    if guess.val == 0 || guess.count == 0
        return pomdp.r_forbidden_action
    end
    r = 0
    if check_guess(pomdp, guess, state)
        r = challenger ? pomdp.r_lose_challenge : pomdp.r_win_challenge
    else
        r = challenger ? pomdp.r_win_challenge : pomdp.r_lose_challenge
    end
    r
end

function r_game_over(pomdp::DudoPomdp, state::DudoState)
    if length(state.my_hand) == 0 && length(state.other_hand) == 0
        return 0
    elseif length(state.my_hand) == 0
        return pomdp.r_lose_game
    elseif length(state.other_hand) == 0
        return pomdp.r_win_game
    end
    error("Game is not over: $state")
end

